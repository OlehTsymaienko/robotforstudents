package net.lafox.robot.commands;

import net.lafox.robot.Command;
import net.lafox.robot.Direction;
import net.lafox.robot.State;

public class PositionCommand implements Command {
    public State execute(State state, String commandString) {
        String[] split = commandString.split("\\s+");
        int x = Integer.parseInt(split[1]);
        int y = Integer.parseInt(split[2]);
        Direction direction = Direction.valueOf(split[3]);

        return new State(x, y, direction);
    }
}
