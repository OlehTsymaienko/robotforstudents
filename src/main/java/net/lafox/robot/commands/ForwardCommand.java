package net.lafox.robot.commands;

import net.lafox.robot.Command;
import net.lafox.robot.State;

public class ForwardCommand implements Command {
    public State execute(State state, String commandString) {
        String[] split = commandString.split("\\s+");
        int steps = Integer.parseInt(split[1]);
        State result = new State(state.getX(), state.getY(), state.getDirection());
        switch (state.getDirection()) {
            case NORTH:
                result.setY(state.getY() + steps);
                break;
            case SOUTH:
                result.setY(state.getY() - steps);
                break;
            case EAST:
                result.setX(state.getX() + steps);
                break;
            case WEST:
                result.setX(state.getX() - steps);
                break;
        }
        return result;
    }
}
