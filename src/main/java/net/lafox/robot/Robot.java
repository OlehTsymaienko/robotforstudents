package net.lafox.robot;

import java.util.Arrays;
import java.util.List;

/**
 * POSITION 1 3 EAST //sets the initial position for the robot as x, y.
 * FORWARD 3 //lets the robot do 3 steps forward
 * WAIT //lets the robot do nothing
 * TURNAROUND //lets the robot turn around
 * FORWARD 1 //lets the robot do 1 step forward
 * RIGHT //lets the robot turn right
 * FORWARD 2 //lets the robot do 2 steps forward
 */

public class Robot {
    private Lang lang = new Lang();
    private State state;

    void executeProgram(String program) {
        List<String> list = split(program);
        for (String command : list) {
            state = lang.executeCommand(state, command);
        }
    }

    private List<String> split(String program) {
        String[] split = program.split("\\n");
        return Arrays.asList(split);
    }

    public State getState() {
        return state;
    }
}
