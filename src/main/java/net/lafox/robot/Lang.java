package net.lafox.robot;


import java.util.HashMap;
import java.util.Map;

import net.lafox.robot.commands.ForwardCommand;
import net.lafox.robot.commands.PositionCommand;
import net.lafox.robot.commands.WaitCommand;

public class Lang {

    private final Map<String, Command> commands;

    public Lang() {
        commands = new HashMap<String, Command>();
        commands.put("POSITION", new PositionCommand());
        commands.put("FORWARD", new ForwardCommand());
        commands.put("WAIT", new WaitCommand());
    }

    State executeCommand(State state, String commandString) {
        String[] split = commandString.split("\\s+");
        String commandName = split[0];
        Command command = commands.get(commandName);
        if (command == null) {
            throw new RuntimeException("Command " + commandName + " not found");
        }
        return command.execute(state, commandString);
    }
}
