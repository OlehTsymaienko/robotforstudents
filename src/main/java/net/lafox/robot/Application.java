package net.lafox.robot;

public class Application {

    public static final String PROGRAM = "POSITION      1 3 EAST //sets the initial position for the robot as x, y.\n" +
            "FORWARD 3 //lets the robot do 3 steps forward\n" +
            "WAIT //lets the robot do nothing\n" +
            "TURNAROUND //lets the robot turn around\n" +
            "FORWARD 1 //lets the robot do 1 step forward\n" +
            "RIGHT //lets the robot turn right\n" +
            "FORWARD 2 //lets the robot do 2 steps forward\n" +
            "\n" +
            "          ";
    public static final String PROGRAM_SHORT = "POSITION      1 3 EAST //sets the initial position for the robot as x, y.\n" +
            "FORWARD 3 //lets the robot do 3 steps forward\n" +
            "WAIT //lets the robot do nothing\n" +
            "";

    public static void main(String[] args) {
        Robot robot = new Robot();
        robot.executeProgram(PROGRAM_SHORT);
        System.out.println("robot.getState() = " + robot.getState());
    }
}
