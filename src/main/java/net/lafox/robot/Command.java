package net.lafox.robot;

public interface Command {
    State execute(State state, String command);
}
