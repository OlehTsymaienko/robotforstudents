package net.lafox.robot;

public enum Direction {
    WEST, NORTH, SOUTH, EAST
}
