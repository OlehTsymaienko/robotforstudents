package net.lafox.robot;

import static org.junit.Assert.assertEquals;

import net.lafox.robot.commands.ForwardCommand;
import net.lafox.robot.commands.PositionCommand;
import org.junit.Test;

public class CommandTest {

    @Test
    public void positionCommandTest() {
        Command command = new PositionCommand();
        State state = command.execute(new State(2, 3, Direction.WEST), "POSITION 5 6 NORTH // comment");
        assertEquals(5, state.getX());
        assertEquals(6, state.getY());
        assertEquals(Direction.NORTH, state.getDirection());
    }

    @Test
    public void forwardCommandTest() {
        Command command = new ForwardCommand();
        State state = command.execute(new State(2, 3, Direction.WEST), "FORWARD 5");
        assertEquals(-3, state.getX());
        assertEquals(3, state.getY());
        assertEquals(Direction.WEST, state.getDirection());
    }
}